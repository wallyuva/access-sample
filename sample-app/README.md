# RBAC Sample App

A sample api that uses rbac to delegate access to routes

## Components

### rbacLoader

A utiltity to load rbac options from an external endpoint (here we are using [sample-rbac-api](../sample-rbac-api/README.md))

### rbacCheck

Restify middleware that uses [easy-rbac](https://github.com/DeadAlready/easy-rbac) to do accesss control checks. Could be customized to generic connect middleware with some error type parameter.

### getRoleFromToken

Restify middleware that uses the `Authorization` header to map the role in the JWT to `req.params.role`.

## userAttributeMapper

Restify middleware that maps some data to `req.params` for ABAC evaluation.