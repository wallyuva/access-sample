'use strict';

const restify = require('restify');
const rbacCheck = require('./rbacCheck');
const getRoleFromToken = require('./getRoleFromToken');
const userAttributeMapper = require('./userAttributeMapper');

let port =  process.env.PORT || 3000;

const server = restify.createServer({
  name: 'rbac-sample-app'
});
server.use(restify.plugins.authorizationParser());

server.get('/resources/:resourceId',
  // we are using a custom middleware here to map some data
  // from the token to the req.params - we can modify our existing
  // getUserFromToken middleware in atom-node-restify to add the role
  getRoleFromToken,
  // we will prob need some way to build extended data for a user
  // like enrollments, learning paths, etc so we can do ABAC callbacks
  userAttributeMapper,
  // we call the middleware here and it knows to look for the access
  // object for ABAC evaluations
  rbacCheck('resource:get'),
  (req, res, next) => {
    res.send({ id: req.params.resourceId });
    next();
  }
);

server.get('/resources',
  getRoleFromToken,
  userAttributeMapper,
  // this check does not care about attributes
  rbacCheck('resource:getAll'),
  (req, res, next) => {
    res.send([
      'a',
      'b',
      'c'
    ]);
    next();
  }
);

server.listen(port, () => {
  console.log(`${server.name} listening at ${port}`);
});
