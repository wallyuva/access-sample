'use strict';

const jwt = require('jsonwebtoken');
const errors = require('restify-errors');

module.exports = function getUserFromToken(req, res, next) {
  if (!req.authorization || !req.authorization.credentials) {
    // return forbidden error if authorization credentials are not in the header
    return next(new errors.ForbiddenError('Invalid or missing authorization token'));
  }

  let decoded = jwt.decode(req.authorization.credentials);
  if (!decoded) {
    return next(new errors.ForbiddenError('Authorization token could not be decoded'));
  }

  if (decoded.user.role) {
    req.params.role = decoded.user.role;
  } else {
    return next(new errors.ForbiddenError('User information was not in the authorization token'));
  }
  return next();
};