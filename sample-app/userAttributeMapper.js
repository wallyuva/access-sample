'use strict';
const request = require('request-promise');
const userAttributeHost = process.env.USERATTRIBUTE_HOST || 'http://localhost:3002';

module.exports = async function(req,res, next) {
  var options = {
    uri: `${userAttributeHost}/user-data`,
    json: true,
    headers: {
      authorization: 'Bearer ' + req.authorization.credentials
    }
  };
  const userAttributeResponse = await request(options);
  req.rbacParams = {
    user: userAttributeResponse,
    request: req.params
  };

  next();
};