'use strict';

const errors = require('restify-errors');

const RBAC = require('easy-rbac');
const rbacLoader = require('./rbacLoader');

module.exports = function rbacCheck(permission) {
  return async (req, res, next) => {

    // load the RBAC config from an external source
    let rbacConfig = await rbacLoader.loadRbacConfig();

    // wire up ABAC config to permissions that require it
    const abacMap = {
      'resource:get': {
        when: async (params) => params.request.resourceId == params.user.userResourceId
      }
    }

    Object.keys(rbacConfig).forEach((key) => {
      rbacConfig[key].can = rbacConfig[key].can.map((canPermission) => {
        if (abacMap[canPermission]) {
          return {
            name: canPermission,
            when: abacMap[canPermission].when
          };
        }

        return canPermission;
      });
    });

    const rbac = RBAC.create(rbacConfig);

    rbac.can(req.params.role, permission, req.rbacParams).then(result => {
      if (result) {
        next();
      } else {
        return next(new errors.UnauthorizedError(
          `RBAC failed for role '${req.params.role}' and permission '${permission}'`));
      }
    });
  }
};