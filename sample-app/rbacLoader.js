'use strict';
const request = require('request-promise');
const rbacDataHost = process.env.RBACDATA_HOST || 'http://localhost:3001';

// map the result into a list of "can" permissions
const mapRbacData = (loadedMappings) => {
  return loadedMappings.reduce((acc, val) => {
    if (!acc[val.role]) {
      acc[val.role] = {
        can: []
      };
    }

    let permissions = [];
    val.resourcePermissions.forEach((resourcePermission) => {
      permissions = permissions.concat(
        resourcePermission.permissions.map(permission => {
          return `${resourcePermission.resourceType}:${permission}`
        }));
    });

    acc[val.role].can = acc[val.role].can.concat(permissions);

    return acc;
  }, {});
};

exports.loadRbacConfig = async () => {
  var options = {
    uri: `${rbacDataHost}/roles/sample-app`,
    json: true
  };
  const loadedMappings = await request(options);
  return mapRbacData(loadedMappings);
};
