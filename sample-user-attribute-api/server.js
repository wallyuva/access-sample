'use strict';

const restify = require('restify');
const getUserIdFromToken = require('./getUserIdFromToken');

let port =  process.env.PORT || 3002;

const server = restify.createServer({
  name: 'rbac-sample-user-attribute-api'
});
server.use(restify.plugins.authorizationParser());

server.get('/user-data',
  getUserIdFromToken,
  (req, res, next) => {
    res.send({
      userResourceId: 'a',
      userId: req.params.userId
    });
    next();
  }
);

server.listen(port, () => {
  console.log(`${server.name} listening at ${port}`);
});
