'use strict';

const restify = require('restify');

let port =  process.env.PORT || 3001;

const server = restify.createServer({
  name: 'rbac-sample-api'
});
server.use(restify.plugins.authorizationParser());

server.get('/roles/:appId',
  (req, res, next) => {
    // the appId is a way to group permissions by the consumer
    // (e.g. content api, etc)
    res.send([
      {
        appId: req.params.appId,
        role: 'student',
        resourcePermissions: [
          { 
            resourceType: 'resource',
            permissions: [
              'get',
              'getAll'
            ]
          }
        ]
      }
    ]);
    next();
  }
);

server.listen(port, () => {
  console.log(`${server.name} listening at ${port}`);
});
