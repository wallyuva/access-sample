# atom-rbac-sample

This is a repository for discovering how to implement RBAC and ABAC
in a node app


## Components

### [sample-app](sample-app/README.md)

A sample api that uses rbac to delegate access to endpoints

###  [sample-rbac-api](sample-rbac-api/README.md)

A sample api that returns rbac config

###  [sample-user-attribute-api](sample-user-attribute-api/README.md)

A sample api that returns user attributes

### Getting started

``` shell
docker-compose up --build
```

Make requests to:
`http://localhost:3000/resources` - access granted
`http://localhost:3000/resources/a` - access granted
`http://localhost:3000/resources/1` - access denied

Shutdown:
``` shell
docker-compose down
```

## To Do List

- [x] Create RBAC sample
- [x] Create ABAC sample
- [x] RBAC api - used to retrieve roles
- [x] User attributes api
- [ ] RBAC config api caching/pub-sub
- [ ] User attributes api caching/pub-sub
- [ ] RBAC consumer permissions broadcasting (api level config)